package five.boxing.timer.ui
import java.util.concurrent.TimeUnit


fun countTimerMusic(duration: Long): String {
    return String.format(
        "%d:%d",
        TimeUnit.MILLISECONDS.toMinutes(duration),
        TimeUnit.MILLISECONDS.toSeconds(duration) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
    )
}


class RoundModel(
    var id: Int,
    var name: String,
    var id_in_app: String,
    var isInApp: Boolean,
    var timer: Long,
    var price: String,
    var image: Int,
    var max_round: Int
)