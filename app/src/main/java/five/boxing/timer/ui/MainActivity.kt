package five.boxing.timer.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.PurchaseInfo
import five.boxing.timer.common.BaseApplication.Companion.bp
import five.boxing.timer.R


class MainActivity : AppCompatActivity(), BillingProcessor.IBillingHandler {

    var h: Handler? = null

    lateinit var image_play: ImageView
    lateinit var image_stop: ImageView
    lateinit var image_next: ImageView
    lateinit var image_love: ImageView
    lateinit var image_setting: ImageView
    lateinit var text_time_count: TextView
    lateinit var text_round: TextView
    lateinit var spinner: Spinner
    lateinit var background: View

    var timer: CountDownTimer? = null
    var timerInit: CountDownTimer? = null
    var timerBreak: CountDownTimer? = null

    var isPlay = true
    var isInitRound = true

    private var timeStart: Long = 180000
    private var timeInit: Long = 6000
    private var countStep = 1
    private var listData: ArrayList<RoundModel> = arrayListOf()
    private var mRoundModels: RoundModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        image_play = findViewById(R.id.image_play)
        image_love = findViewById(R.id.image_love)
        image_stop = findViewById(R.id.image_stop)
        image_next = findViewById(R.id.image_next)
        image_setting = findViewById(R.id.image_setting)
        text_time_count = findViewById(R.id.text_time_count)
        text_round = findViewById(R.id.text_round)
        spinner = findViewById(R.id.spinner)
        background = findViewById(R.id.background)
        bp = BillingProcessor(this, "", this)
        bp!!.initialize()
        text_round.text = "Round ${countStep}"

        image_stop.visibility = View.INVISIBLE
        image_play.setOnClickListener {
            if (isInitRound) {
                startInitCount(timeInit)
                isInitRound = false
                image_play.isEnabled = false
                return@setOnClickListener
            }

            if (isPlay) {
                isPlay = false
                image_play.setImageResource(R.drawable.ic_baseline_pause_circle_outline_24)
                image_stop.visibility = View.INVISIBLE
                startCount(timeStart)
            } else {
                timer?.cancel()
                isPlay = true
                image_play.setImageResource(R.drawable.ic_baseline_play_circle_filled_24)
                image_stop.visibility = View.VISIBLE
            }
        }

        image_stop.setOnClickListener {
            timerInit?.cancel()
            timerBreak?.cancel()
            timer?.cancel()
            text_time_count.text = "03:00"
            image_play.setImageResource(R.drawable.ic_baseline_play_circle_filled_24)
            image_stop.visibility = View.INVISIBLE
            countStep = 1
        }


        image_setting.setOnClickListener {
            if (isPlay) {
                startActivity(Intent(this, SettingActivity::class.java))
            }
        }


        image_love.setOnClickListener {
            startActivity(Intent(this, LoveActivity::class.java))
        }
    }

    private fun setupSpinner() {
        listData = arrayListOf()
        listData.add(
            RoundModel(
                1,
                "Classic Boxing",
                "android.test.purchased",
                false,
                180000,
                "Free",
                R.drawable.b1,
                12
            )
        )
        listData.add(
            RoundModel(
                2,
                "Amateur",
                "android.test.purchased",
                false,
                120000,
                "Free",
                R.drawable.b2,
                4
            )
        )
        listData.add(
            RoundModel(
                3,
                "MMA",
                "boxing.pack.one",
                true,
                3000000,
                "200.000 vnđ",
                R.drawable.b3,
                5
            )
        )
        listData.add(
            RoundModel(
                4,
                "MMA Advanced",
                "boxing.pack.two",
                true,
                4000000,
                "500.000 vnđ",
                R.drawable.b4,
                7
            )
        )
        listData.add(
            RoundModel(
                5,
                "Amateur Advanced",
                "boxing.pack.three",
                true,
                3000000,
                "1.000.000 vnđ",
                R.drawable.b5,
                8
            )
        )

        val customAdapter = CustomAdapter(this, listData)
        spinner.apply {
            adapter = customAdapter
        }

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                pos: Int,
                id: Long
            ) {
                if (parent.getItemAtPosition(pos) != null) {
                    mRoundModels = parent.getItemAtPosition(pos) as RoundModel
                    if (mRoundModels!!.isInApp) {
                        bp!!.purchase(this@MainActivity, mRoundModels!!.id_in_app)
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }


    private fun startBreakRound(time: Long) {
        timerBreak = object : CountDownTimer(time, 1000) {
            @SuppressLint("ResourceAsColor")
            override fun onTick(millisUntilFinished: Long) {
                Log.d("TimerExample", "Going for... ${countTimerMusic(millisUntilFinished)}")
                text_time_count.text = countTimerMusic(millisUntilFinished)
            }

            @SuppressLint("ResourceAsColor")
            override fun onFinish() {
                startSound("sound_keng.mp3")
                countStep++
                text_round.text = "Round ${countStep}"
                isPlay = true
                if (isPlay) {
                    isPlay = false
                    image_play.setImageResource(R.drawable.ic_baseline_pause_circle_outline_24)
                    image_stop.visibility = View.INVISIBLE
                    startCount(180000)
                } else {
                    timer?.cancel()
                    isPlay = true
                    image_play.setImageResource(R.drawable.ic_baseline_play_circle_filled_24)
                    image_stop.visibility = View.VISIBLE
                }
            }
        }
        timerBreak!!.start()
    }


    private fun startInitCount(time: Long) {
        timerInit = object : CountDownTimer(time, 1000) {
            @SuppressLint("ResourceAsColor")
            override fun onTick(millisUntilFinished: Long) {
                Log.d("TimerExample", "Going for... ${countTimerMusic(millisUntilFinished)}")
                text_time_count.text = countTimerMusic(millisUntilFinished)
            }

            @SuppressLint("ResourceAsColor")
            override fun onFinish() {
                image_play.isEnabled = true
                startSound("sound_keng.mp3")
                if (isPlay) {
                    isPlay = false
                    image_play.setImageResource(R.drawable.ic_baseline_pause_circle_outline_24)
                    image_stop.visibility = View.INVISIBLE
                    startCount(timeStart)
                } else {
                    timer?.cancel()
                    isPlay = true
                    image_play.setImageResource(R.drawable.ic_baseline_play_circle_filled_24)
                    image_stop.visibility = View.VISIBLE
                }
            }
        }
        timerInit!!.start()
    }


    fun startCount(time: Long) {
        timer = object : CountDownTimer(time, 1000) {
            @SuppressLint("ResourceAsColor")
            override fun onTick(millisUntilFinished: Long) {
                Log.d("TimerExample", "Going for... ${countTimerMusic(millisUntilFinished)}")
                text_time_count.text = countTimerMusic(millisUntilFinished)
                background.background = resources.getDrawable(R.drawable.bgr_fit, null)
                timeStart = millisUntilFinished
                if (countTimerMusic(millisUntilFinished) == "0:10") {
                    startSound("sound_warninng_new.mp3")
                }
            }

            @SuppressLint("ResourceAsColor")
            override fun onFinish() {
                background.setBackgroundColor(R.color.red)
                startSound("boxing_end.mp3")
                startBreakRound(6000)
            }
        }
        timer!!.start()
    }

    fun startSound(nameSound: String) {
        val mPlayer = MediaPlayer()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mPlayer.setDataSource(assets.openFd(nameSound))
        }
        mPlayer.prepare()
        mPlayer.start()
    }

    fun start() {
        h = Handler()
        h?.postDelayed(object : Runnable {
            private var time: Long = 0
            override fun run() {
                time += 1000
                Log.d("TimerExample", "Going for... $time")
                h?.postDelayed(this, 1000)

            }
        }, 1000)
    }

    override fun onProductPurchased(productId: String, details: PurchaseInfo?) {

        bp!!.consumePurchaseAsync(productId, object : BillingProcessor.IPurchasesResponseListener {
            override fun onPurchasesSuccess() {
                Toast.makeText(this@MainActivity, "Success", Toast.LENGTH_SHORT).show()
            }

            override fun onPurchasesError() {
                Log.d("ddd", "eeee")
                Toast.makeText(this@MainActivity, "Error payment", Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onPurchaseHistoryRestored() {

    }

    override fun onBillingError(errorCode: Int, error: Throwable?) {

    }

    override fun onBillingInitialized() {

    }

    override fun onResume() {
        super.onResume()
        setupSpinner()

    }

    override fun onPause() {
        super.onPause()
        timer?.cancel()
        timerInit?.cancel()
        timerBreak?.cancel()
        text_time_count.text = "03:00"
        image_play.setImageResource(R.drawable.ic_baseline_play_circle_filled_24)
        image_stop.visibility = View.INVISIBLE
        countStep = 1
    }
}