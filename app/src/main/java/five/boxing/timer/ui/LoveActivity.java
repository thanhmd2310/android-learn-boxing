package five.boxing.timer.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.PurchaseInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import five.boxing.timer.R;


public class LoveActivity extends AppCompatActivity implements BillingProcessor.IBillingHandler {
    BillingProcessor bp;
    private List<String> listIdInApp = new ArrayList<>();
    private List<String> listIdSubInApp = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_love);
        listIdInApp.add("final_inapp_1");
        listIdInApp.add("final_inapp_2");
        listIdInApp.add("final_inapp_3");
        listIdInApp.add("final_inapp_4");
        listIdInApp.add("final_inapp_5");

        listIdSubInApp.add("sub_inapp_1");
        listIdSubInApp.add("sub_inapp_2");
        listIdSubInApp.add("sub_inapp_3");
        listIdSubInApp.add("sub_inapp_4");
        listIdSubInApp.add("sub_inapp_5");

        Objects.requireNonNull(getSupportActionBar()).hide();

        bp = new BillingProcessor(this, "", this);
        bp.initialize();

        findViewById(R.id.btn_donate_1).setOnClickListener(view -> {
            bp.purchase(this, listIdInApp.get(0));
        });

        findViewById(R.id.btn_donate_2).setOnClickListener(view -> {
            bp.purchase(this, listIdInApp.get(1));
        });
        findViewById(R.id.btn_donate_3).setOnClickListener(view -> {
            bp.purchase(this, listIdInApp.get(2));
        });

        findViewById(R.id.btn_donate_4).setOnClickListener(view -> {
            bp.purchase(this, listIdInApp.get(3));
        });

        findViewById(R.id.btn_donate_5).setOnClickListener(view -> {
            bp.purchase(this, listIdInApp.get(4));
        });

        findViewById(R.id.text_policy).setOnClickListener(view -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.termsfeed.com/live/55130668-f6a7-495e-a689-b4df753bd0e3"));
            startActivity(browserIntent);
        });

        //sub
        findViewById(R.id.layout_sub_1).setOnClickListener(v -> {
            bp.subscribe(this, listIdSubInApp.get(0));
        });
        //sub
        findViewById(R.id.layout_sub_2).setOnClickListener(v -> {
            bp.subscribe(this, listIdSubInApp.get(1));

        });
        //sub
        findViewById(R.id.layout_sub_3).setOnClickListener(v -> {
            bp.subscribe(this, listIdSubInApp.get(2));

        });
        //sub
        findViewById(R.id.layout_sub_4).setOnClickListener(v -> {
            bp.subscribe(this, listIdSubInApp.get(3));
        });
        //sub
        findViewById(R.id.layout_sub_5).setOnClickListener(v -> {
            bp.subscribe(this, listIdSubInApp.get(4));

        });


        findViewById(R.id.image_back).setOnClickListener(v -> {
            finish();
        });
    }

    @Override
    public void onDestroy() {
        if (bp != null) {
            bp.release();
        }
        super.onDestroy();
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable PurchaseInfo details) {
        Toast.makeText(this, "Thanks DONATE!!!", Toast.LENGTH_SHORT).show();
        bp.consumePurchaseAsync(productId, new BillingProcessor.IPurchasesResponseListener() {
            @Override
            public void onPurchasesSuccess() {
                Toast.makeText(LoveActivity.this, "next", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPurchasesError() {
                Toast.makeText(LoveActivity.this, "eeee", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onPurchaseHistoryRestored() {
    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
    }

    @Override
    public void onBillingInitialized() {
    }
}