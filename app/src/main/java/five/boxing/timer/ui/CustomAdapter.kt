package five.boxing.timer.ui
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import five.boxing.timer.R

class CustomAdapter(
    private val context: Context,
    val mRoundModel: List<RoundModel>
) :
    BaseAdapter() {

    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemHolder
        if (convertView == null) {
            view = inflater.inflate(R.layout.spinner_item, parent, false)
            vh = ItemHolder(view)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemHolder
        }
        vh.text_name.text = mRoundModel[position].name
        vh.price.text = mRoundModel[position].price
        vh.image_donate.setImageResource(mRoundModel[position].image)


//        vh.layout_1.setOnClickListener {
//            listener.onSuccess(mRoundModel[position])
//        }

        return view
    }

    override fun getCount(): Int {
        return mRoundModel.size
    }

    override fun getItem(p0: Int): Any {
        return mRoundModel[p0]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong();
    }


    private class ItemHolder(row: View?) {
        val price: TextView
        val text_name: TextView
        val image_donate: ImageView
        val layout_1: View

        init {
            price = row?.findViewById(R.id.price) as TextView
            text_name = row.findViewById(R.id.text_name) as TextView
            image_donate = row.findViewById(R.id.image_avatar) as ImageView
            layout_1 = row.findViewById(R.id.layout_1) as View
        }
    }

    interface IClickItem {
        fun onSuccess(mRoundModel: RoundModel)
    }
}