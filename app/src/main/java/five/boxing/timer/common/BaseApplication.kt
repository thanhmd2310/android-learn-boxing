package five.boxing.timer.common
import android.app.Application
import com.anjlab.android.iab.v3.BillingProcessor

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        StorageService.create(getSharedPreferences("local_storage", MODE_PRIVATE))



    }

    companion object {
        var bp: BillingProcessor? = null

    }
}